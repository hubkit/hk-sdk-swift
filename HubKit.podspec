Pod::Spec.new do |s|
  s.name                    = 'HubKit'
  s.version                 = '0.16.0'
  s.summary                 = 'HubKit Framework'
  s.homepage                = 'https://gitlab.com/hubkit/hk-sdk-swift'
  s.license                 = { :type => "MIT", :file => "LICENSE" }
  s.author                  = { "Move Upwards" => "contact@moveupwards.com" }
  s.source                  = { :git => 'git@gitlab.com:hubkit/hk-sdk-swift.git', :tag => s.version }
  s.swift_version           = '5.0'
  s.ios.deployment_target   = '10.0'
  s.osx.deployment_target   = '10.13'
  s.source_files            = 'HubKit/**/*'
  s.frameworks              = "Foundation"

  s.dependency 'Offenbach'
end
