//
//  HKManagerTests.swift
//  HubKitTests
//
//  Created by Loïc GRIFFIE on 24/09/2018.
//  Copyright © 2018 Loïc GRIFFIE. All rights reserved.
//

import XCTest
import Alamofire
import Offenbach
@testable import HubKit

class HKManagerTests: XCTestCase {
    var apiManager: HKManager?

    override func setUp() {
        super.setUp()

        apiManager = HKManager()
        apiManager?.set(config: Config(baseURL: "http://localhost:8080/api/v1"))
    }

    override func tearDown() {
        super.tearDown()
    }
}
