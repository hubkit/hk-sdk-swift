# Change Log

All notable changes to this project will be documented in this file.
`HubKit` adheres to [Semantic Versioning](https://semver.org/).

## [0.15.0](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.15.0)

Update Podspec
Update CHANGELOG
> Released on 2018-11-07

## [0.14.0](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.14.0)

Update README
Update CHANGELOG
Remove useless single get route
> Released on 2018-10-29

## [0.13.0](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.13.0)

Update README
Update CHANGELOG
Add setter to define current project
Add getter to have the project's devices list
> Released on 2018-10-23

## [0.12.0](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.12.0)

Update README
Update CHANGELOG
Add documentation
> Released on 2018-10-19

## [0.11.0](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.11.0)

Add metas parameters for session create
> Released on 2018-10-16

## [0.10.0](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.10.0)

Remove activity from session
> Released on 2018-10-16

## 0.9.x Releases

- `0.9.x` Releases - [0.9.3](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.9.3) | [0.9.2](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.9.2) | [0.9.1](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.9.1)

### [0.9](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.9)

Add external device UUID
JSON encode create session
Remove session state on create
> Released on 2018-10-12

## 0.8.x Releases

- `0.8.x` Releases - [0.8.5](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.8.5) | [0.8.4](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.8.4) | [0.8.3](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.8.3) | [0.8.2](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.8.2) | [0.8.1](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.8.1)

## [0.8](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.8)

Remove device > project relation
> Released on 2018-10-08

## [0.7](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.7)

Remove device > project relation
> Released on 2018-10-08

## [0.4](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.4)

Add Project list in HKAccount
Add Device list in HKProject
> Released on 2018-09-26

## [0.3](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.2)

Add Project list in HKAccount
Add Device list in HKProject
> Released on 2018-09-26

## [0.2](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.2)

Complete documentation
> Released on 2018-09-26

## [0.1](https://gitlab.com/hubkit/hk-sdk-swift/tags/0.1)

> Released on 2018-09-25