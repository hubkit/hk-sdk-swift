//
//  HKAccountRoutes.swift
//  HubKit
//
//  Created by Loïc GRIFFIE on 24/09/2018.
//  Copyright © 2018 Loïc GRIFFIE. All rights reserved.
//

import Foundation
import Alamofire

extension HKAccount {
    /// Get the current authenticated user
    public static func me<T: Decodable>(completion: @escaping (Result<T, Error>) -> Void) {
        HKManager.default.get(action: "me", completion: completion)
    }
}
