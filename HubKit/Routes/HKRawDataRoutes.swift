//
//  HKRawDataRoutes.swift
//  HubKit
//
//  Created by Loïc GRIFFIE on 25/09/2018.
//  Copyright © 2018 Loïc GRIFFIE. All rights reserved.
//

import Foundation
import Alamofire

extension HKRawData {
    /// Create a new session
    public static func create<T: Decodable>(in session: HKSession,
                                            _ device: HKDevice,
                                            _ file: HKFile,
                                            progress: @escaping (Double) -> Void,
                                            completion: @escaping (Result<T, Error>) -> Void) {
        let parameters: Parameters = [
            "session": session.identifier,
            "device": device.identifier
        ]

        HKManager.default.upload(action: "raw_datas",
                                 parameters: parameters,
                                 file: file,
                                 progress: progress,
                                 completion: completion)
    }

    /// Get the raw data for the given identifier
    public static func get<T: Decodable>(identifier: String, completion: @escaping (Result<T, Error>) -> Void) {
        HKManager.default.get(action: "raw_datas/\(identifier)", completion: completion)
    }
}
