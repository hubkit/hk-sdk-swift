//
//  HubKit.h
//  HubKit
//
//  Created by Loïc GRIFFIE on 24/09/2018.
//  Copyright © 2018 Loïc GRIFFIE. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for HubKit.
FOUNDATION_EXPORT double HubKitVersionNumber;

//! Project version string for HubKit.
FOUNDATION_EXPORT const unsigned char HubKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <HubKit/PublicHeader.h>


