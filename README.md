# HubKit

[![Build Status](https://travis-ci.org/Alamofire/Alamofire.svg?branch=master)](https://travis-ci.org/Alamofire/Alamofire)
[![CocoaPods Compatible](https://img.shields.io/cocoapods/v/HubKit.svg)](https://img.shields.io/cocoapods/v/HubKit.svg)
[![Platform](https://img.shields.io/cocoapods/p/HubKit.svg?style=flat)](https://alamofire.github.io/HubKit)

HubKit is library written in Swift to communicate with hubkit.io.

- [HubKit](#hubkit)
    - [Features](#features)
    - [Requirements](#requirements)
    - [Component Libraries](#component-libraries)
    - [Installation](#installation)
        - [CocoaPods](#cocoapods)
    - [Usage](#usage)
    - [Credits](#credits)
    - [License](#license)

## Features

- [x] Easy setup and configuration
- [x] Comprehensive Unit Test Coverage
- [x] [Complete Documentation](https://doc.hubkit.cloud/ios/index.html)

## Requirements

- iOS 11.0+ / macOS 10.10+ / tvOS 9.0+ / watchOS 2.0+
- Xcode 10.0+
- Swift 4.1+

## Component Libraries

In order to keep HubKit focused specifically on its core functionnality implementations, additional component libraries are needed.

- [Alamofire](https://github.com/Alamofire/Alamofire) - HTTP networking library written in Swift.
- [AlamofireObjectMapper](https://github.com/tristanhimmelman/AlamofireObjectMapper) - An Alamofire extension which converts JSON response data into swift objects using ObjectMapper.
- [SwiftLint](https://github.com/realm/SwiftLint) - A tool to enforce Swift style and conventions.

## Installation

### CocoaPods

[CocoaPods](https://cocoapods.org) is a dependency manager for Cocoa projects. You can install it with the following command:

```bash
$ gem install cocoapods
```

To integrate Alamofire into your Xcode project using CocoaPods, specify it in your `Podfile`:

```ruby
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '10.0'
use_frameworks!

target '<Your Target Name>' do
    pod 'HubKit'
end
```

Then, run the following command:

```bash
$ pod install
```

## Usage

```swift
import Foundation
import HubKit

final class HubKitService {
    var account: HKAccount?

    init() {
        // Define your environment
        let config = HKConfig(environment: .prod, .v1)

        // Define your project Api Key
        let apiKey = HKToken(apiKey: "PROJECT-API-KEY")

        // Set your config and api key
        HKManager.default.set(config: config).set(token: apiKey).set(project: "YOUR-PROJECT-IDENTIFIER")

        // Check the current user
        HKAccount.me(success: { [weak self] account in
            self?.account = account
        }, failure: { error in
            print(error.localizedDescription)
        })
    }
}
```

## Credits

HubKit is owned and maintained by the SURF'IN Company.

## License

HubKit is released under the MIT license. [See LICENSE](https://gitlab.com/hubkit/hk-sdk-swift/blob/master/LICENSE) for details.